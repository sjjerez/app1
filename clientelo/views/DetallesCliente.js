import React from 'react';
import { Alert, StyleSheet, View, } from 'react-native';
import { Headline, Text, Subheading, Button, FAB} from 'react-native-paper';
import globalStyles from '../styles/global'; 
import axios from 'axios';
import { useEffect, useState } from 'react';

const DetallesCliente = ({navigation,route}) => {
    // console.log(route.params);
    
    const guardarConsultarAPI = route.params.guardarConsultarAPI;
 
    const cliente = route.params.cliente; 


    const mostrarConfirmacion = () => {
        Alert.alert(
            '¿Deseas eliminar este cliente?',
            'Un contacto eliminado no se puede recuperar',
            [
                {text: 'Si, Eliminar', onPress: () => eliminarContacto()},
                {text: 'Cancelar', style:'cancel'}
            ]
        )
    }

    const eliminarContacto = async () => {
        // console.log('Eliminando...', id)
        const url = `http://10.0.2.2:3000/clientes/${cliente.id}`;
        try {
            await axios.delete(url);            
        } catch (error) {
            console.log(error);
        }

        // Redireccionamos 
        navigation.navigate('Inicio');
 
        // Cambiamos el estate para actualizar la vista de clientes
        guardarConsultarAPI(true); 
    }


    return ( 
        <>
        <View style={globalStyles.contendor, {marginLeft:20, marginRight:20}}>
            <Headline style={globalStyles.titulo}>{cliente.nombre1}</Headline>
            
            {cliente.nombre2!=='' 
                ? <Text style={styles.texto}>Nombre pareja: <Subheading>{cliente.nombre2}</Subheading></Text> 
            : null}
            <Text style={styles.texto}>Teléfono: <Subheading>{cliente.telefono}</Subheading></Text>
            <Text style={styles.texto}>Correo: <Subheading>{cliente.correo}</Subheading></Text>
            <Text style={styles.texto}>Fecha: <Subheading>{cliente.fecha}</Subheading></Text>
            <Text style={styles.texto}>Tipo de Evento: <Subheading>{cliente.tipoEvento}</Subheading></Text>
            <Text style={styles.texto}>Localidad: <Subheading>{cliente.localidad}</Subheading></Text>
            <Text style={styles.texto}>Ciudad: <Subheading>{cliente.ciudad}</Subheading></Text>
            <Text style={styles.texto}>Lugar: <Subheading>{cliente.lugar}</Subheading></Text>
            <Text style={styles.texto}>Servicios deseados: <Subheading>
                {cliente.serviciosDeseados["DJ"] ? "  DJ" : null}
                {cliente.serviciosDeseados["Fotomatón"] ? "  Fotomatón" : null}
                {cliente.serviciosDeseados["Mesa Dulce"] ? "  Mesa dulce" : null}
                </Subheading></Text>
            {/* <Text style={styles.texto}>Servicios deseados: {cliente.serviciosDeseados.map(
                servicio => <Subheading>{servicio}</Subheading>)} </Text> */}
            
            <Text style={styles.texto}>Presupuesto: <Subheading>{cliente.presupuesto}</Subheading></Text>

            <Button 
                style={styles.boton} 
                mode="contained" 
                icon="cancel"
                onPress={() => mostrarConfirmacion() }
            >
                Eliminar Cliente
            </Button> 
        </View>
        
        <FAB 
            icon="pencil"
            style={globalStyles.fab}
            onPress={() => navigation.navigate("NuevoCliente",  { cliente: cliente, guardarConsultarAPI} )}
        />
        </>
     );
}

const styles = StyleSheet.create ({
    texto: {
        marginBottom: 20,
        fontSize: 18
    },
    boton: {
        marginTop: 100,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#C22222'
    }
})
 
export default DetallesCliente;