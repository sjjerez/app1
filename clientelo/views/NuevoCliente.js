import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { Text, Keyboard, View, StyleSheet } from 'react-native';  
import BarraProgreso from '../components/formulario/BarraProgreso';
import Paso1 from '../components/formulario/Paso1'; 
import Paso2 from '../components/formulario/Paso2'; 
import Paso3 from '../components/formulario/Paso3'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



const NuevoCliente = ({navigation, route}) => {
    
    const guardarConsultarAPI = route.params.guardarConsultarAPI;

    
    // Selección de paso activo:
    // -------------------------------------------------------------------------
    const [ pasoActivo, guardarPasoActivo] = useState(0);  
    
    const imprimePasoActivo = (pa) => { 
        if (pa === 0){
            return <Paso1 nombre1={nombre1} nombre2={nombre2} telefono={telefono} correo={correo} 
                         guardarNombre1={guardarNombre1}  guardarNombre2={guardarNombre2}  
                         guardarTelefono={guardarTelefono}  guardarCorreo ={guardarCorreo} 
                    ></Paso1>
        } else if (pa === 1){
            return <Paso2 fecha={fecha} tipoEvento={tipoEvento} ciudad={ciudad} lugar={lugar}
            localidad={localidad}
            guardarFecha={guardarFecha} guardarTipoEvento={guardarTipoEvento}
            guardarCiudad={guardarCiudad} guardarLugar={guardarLugar} guardarLocalidad={guardarLocalidad}
                    ></Paso2>
        } else{
            return <Paso3 servicios={servicios} serviciosDeseados={serviciosDeseados} presupuesto={presupuesto}
                    guardarServiciosDeseados = {guardarServiciosDeseados} guardarPresupuesto={guardarPresupuesto}></Paso3>
        }
    } 

    
    // Detectamos si estamos editando un cliente o creando uno nuevo
    // -------------------------------------------------------------------------
    useEffect( () =>  {
        // Si lo estamos editando, capturamos primero los valores existentes:
        if(route.params.cliente){
            console.log("Editando cliente...");
            const {nombre1, telefono, correo} = route.params.cliente;
            guardarNombre1(nombre1);
            guardarNombre2(nombre2);
            guardarTelefono(telefono);
            guardarCorreo(correo);
            guardarFecha(fecha);
            guardarTipoEvento(tipoEvento);
            guardarLocalidad(localidad);
            guardarCiudad(ciudad);
            guardarLugar(lugar);
            guardarServiciosDeseados(serviciosDeseados);
            guardarPresupuesto(presupuesto);
        }else {
            // Nuevo cliente
            console.log("Nuevo cliente...");
            
        }
    },[]);


 

    // Campos del formulario
    // -------------------------------------------------------------------------
    



    // Paso 1
    const [nombre1, guardarNombre1] = useState('');
    const [nombre2, guardarNombre2] = useState('');
    const [telefono, guardarTelefono] = useState('');
    const [correo, guardarCorreo] = useState(''); 

    // Paso 2
    const [fecha, guardarFecha] = useState('');
    const [tipoEvento, guardarTipoEvento] = useState('');
    const [localidad, guardarLocalidad] = useState('');
    const [ciudad, guardarCiudad] = useState('');
    const [lugar, guardarLugar] = useState('');

    // Paso 3
    const [serviciosDeseados, guardarServiciosDeseados] = useState({
        "DJ":false,
        "Fotomatón": false,
        "Mesa dulce": false});
    const [presupuesto, guardarPresupuesto] = useState(0);        
     


    // GUARDADO DE CLIENTE EN LA BBDD
    // -------------------------------------------------------------------------

    // Estate (Flag) para activar el cambio
    const [guardar, guardarGuardar] = useState(false);


    // Use efect para aplicar efectos tras ciertos cambios
    useEffect(() => {
        if(guardar) {
            // console.log("guardando desde effects"); 
            if(serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
                resultado=950;
            }else if(serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
                resultado=800;            
            }else if(serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
                resultado=450;            
            }else if(!serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
                resultado=450;            
            }else if(!serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
                resultado=300;            
            }else if(serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
                resultado=700;            
            }else if(!serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
                resultado=700;            
            }
            guardarPresupuesto(resultado);  
            guardarCliente();  
            guardarGuardar(false);
        }
    }, [guardar]);
    
     

    // Función para Almacenar cliente en la BD
    const guardarCliente = async () => {
        
        // // Validación
        // if(nombre1 === '' || (botonActivo===1 && nombre2 === '') || telefono === '' || correo === ''){
             
        //     // console.log('Hay campos vacíos');
        //     guardarAlerta(true);
        //     return
        // }
    
        
        // if (botonActivo===0 ) {
        //     console.log("cambiamos el 2 a null") ;
        //     guardarNombre2("dsasdg");
        // }

        // Generación de cliente
        const cliente = { nombre1, nombre2, telefono, correo, fecha, tipoEvento, 
            localidad, ciudad, lugar, serviciosDeseados, presupuesto};
        console.log(cliente);

        // Si estamos editando o creando un nuevo cliente
        if(route.params.cliente){
            // Estamos editando
            const {id} = route.params.cliente;
            cliente.id = id;
            const url = `http://10.0.2.2:3000/clientes/${id}`; 

            try {
                await axios.put(url, cliente);
            } catch (error) {
                console.log(error);
            }
        }else{
            // Estamos creando uno nuevo
         // Guardado del cliente en la API
            try{
                // URL específica de android
                await axios.post('http://10.0.2.2:3000/clientes', cliente);
            } catch (error){
                console.log(error);
            }
        }

        // Redirección
        navigation.navigate('Inicio');

        // Limpiar el formulario (opcional)
        guardarNombre1('');
        guardarNombre2('');
        guardarTelefono('');
        guardarCorreo('');

        // console.log('Guardando cliente');

        // Cambiamos el estate para actualizar la vista de clientes
        guardarConsultarAPI(true);
    }


    
    // CONSULTA A LA BBDD POR LOS SERVICIOS
    // -------------------------------------------------------------------------
    
    // Variable donde guardamos temporalmente los servicios de la bd.
    const [ servicios, guardarServicios] = useState([]);  
    
    // Disparador para saber si tenemos que comprobar 
    const [consultarServicios, guardarConsultarServicios ] = useState(true); 

    // Flag de consulta de API: Dispararemos una consulta cada vez que esta
    // etiqueta sea activada.
    useEffect(() => {

        // Función para obtener todos los servicios de la base de datos
        const obtenerServiciosApi = async() => {
            
            // Si entramos aquí, es porque se ha activado el flag 'consultarServicios 
            try{
                // Petición
                const resultado = await axios.get('http://10.0.2.2:3000/servicios');
                
                // Actualización de la variable local
                guardarServicios(resultado.data);
                 

                // Bloqueamos el flag 
                guardarConsultarServicios(false);

            } catch(error) { console.log(error); }
        }

        // Disparador
        if(consultarServicios){  
            obtenerServiciosApi(); 
        } 
    }, [consultarServicios]);

    return (  
        <>
            <KeyboardAwareScrollView onPress={Keyboard.dismiss}>
                
                {imprimePasoActivo(pasoActivo)} 
                
                
            </KeyboardAwareScrollView>
             
            <BarraProgreso pasoActivo={pasoActivo} 
                guardarPasoActivo={guardarPasoActivo} 
                guardarGuardar={guardarGuardar}
            />  
        </>
     );
}
 

// Estilos propios de este formulario
const styles = StyleSheet.create({
    

});

export default NuevoCliente;