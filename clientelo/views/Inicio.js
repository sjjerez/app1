import React, { useEffect, useState } from 'react';
import { Text, FlatList, View, StyleSheet, LogBox} from 'react-native';
import axios from 'axios';
import { List, Headline, Button, FAB, Searchbar } from 'react-native-paper';
import globalStyles from '../styles/global'


import TarjetaCliente from '../components/ui/TarjetaCliente';

const Inicio = ({navigation}) => {

    
LogBox.ignoreLogs([
    'Non-serializable values were found in the navigation state',
    'DevTools failed to load SourceMap',
    'Remote debugger is in a background tab which may cause apps to perform slowly',
    'componentWillReceiveProps has been renamed, and is not recommended for use'
  ]);


    // CONSULTA A LA BBDD POR LOS CLIENTES
    // -------------------------------------------------------------------------
    
    // Variable donde guardamos temporalmente los clientes de la bd.
    const [ clientes, guardarClientes] = useState([]);  
    
    // Disparador para saber si tenemos que comprobar 
    const [consultarAPI, guardarConsultarAPI ] = useState(true); 

    // Flag de consulta de API: Dispararemos una consulta cada vez que esta
    // etiqueta sea activada.
    useEffect(() => {

        // Función para obtener todos los clientes de la base de datos
        const obtenerClientesApi = async() => {
            
            // Si entramos aquí, es porque se ha activado el flag 'consultarAPI 
            try{
                // Petición
                const resultado = await axios.get('http://10.0.2.2:3000/clientes');
                
                // Actualización de la variable local
                guardarClientes(resultado.data);

                // Bloqueamos el flag 
                guardarConsultarAPI(false);

            } catch(error) { console.log(error); }
        }

        // Disparador
        if(consultarAPI){ 
            // console.log("Consultado API...");
            obtenerClientesApi(); }
    }, [consultarAPI]);


    // VISTA INICIO
    // -------------------------------------------------------------------------
    return ( 
        
        // Contenedor principal
        <View style={globalStyles.contenedor}> 
            
            {/* Cabecera */}
            <Headline style={globalStyles.titulo}>{ clientes.length > 
                0 ? "Clientes" : "Aún no hay clientes"} 
            </Headline>

            
            <Searchbar
                placeholder="Buscar..."
                // onChangeText={onChangeSearch}
                // value={searchQuery}
                style={styles.barraBusqueda}
            />

            {/* Iteración del listado de clientes */}
            <FlatList 
                data={clientes}
                keyExtractor = { cliente => (cliente.id).toString() }
                renderItem = { ({item}) => (

                    // Mostramos tarjeta de cliente
                    <TarjetaCliente navigation={navigation} cliente={item} guardarConsultarAPI={guardarConsultarAPI} />
                )}
            />

            {/* Boton inferior flotante: añadir cliente */}
            <FAB 
                icon="account-plus"
                style={globalStyles.fab}
                onPress={() => navigation.navigate("NuevoCliente", {guardarConsultarAPI} )}
            />
            

        </View>

     );
}
 
// ESTILOS DE COMPONENTE
// -----------------------------------------------------------------------------
const styles = StyleSheet.create({
    barraBusqueda: {
        marginBottom:10, 
        marginLeft:10,
        marginRight:10,
        borderRadius:30
    }
  });
 
export default Inicio;