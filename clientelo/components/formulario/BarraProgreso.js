import React, {useState, useEffect} from 'react'; 
import {Text, View, Keyboard, StyleSheet, Dimensions } from 'react-native';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';




const BarraProgreso = ({pasoActivo, guardarPasoActivo, guardarGuardar}) => {

    
    // console.log("Estamos en el paso ", pasoActivo);


    // Mecanismo para ocultar la barra de progreso
    // -------------------------------------------------------------------------
    const [keyboard, setKeyboard] = useState(Boolean);

    useEffect(() => {
        Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
        Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

        return () => {
        Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
        Keyboard.addListener('keyboardDidHide', _keyboardDidHide);
        };
    }, []);

    const _keyboardDidShow = () => { setKeyboard(true); };

    const _keyboardDidHide = () => { setKeyboard(false); };
    


    return ( 
        <View style = 
            {keyboard 
                ? [styles.mainBox,  styles.oculto]
                : [styles.mainBox]
            }

            >
            <ProgressSteps 
                marginBottom={pasoActivo} > 
                
                <ProgressStep 
                    nextBtnText="Siguiente"
                    onNext= {()=> guardarPasoActivo(pasoActivo + 1)}
                />
                
                
                <ProgressStep 
                    previousBtnText="Atrás"
                    onPrevious= {()=> guardarPasoActivo(pasoActivo - 1)} 
                    nextBtnText="Siguiente"
                    onNext= {()=> guardarPasoActivo(pasoActivo + 1)}
                />
                
                
                <ProgressStep
                    previousBtnText="Atrás"
                    onPrevious= {()=> guardarPasoActivo(pasoActivo - 1)} 
                    finishBtnText="Guardar Cliente" 
                    onSubmit= {()=> guardarGuardar(true)}
                />  
            </ProgressSteps>
        </View>
     );
    
}
 
const styles = StyleSheet.create({
    mainBox : {
        width: Dimensions.get('window').width,
        position: 'absolute',
        left: 0,
        bottom: -10, 
    },
    oculto:{ transform: [{ scale: 0 }] }

});
 
export default BarraProgreso;