import React, {useState} from 'react';
import { Button, View, StyleSheet} from 'react-native';
import { Text, Subheading, TextInput, Headline, Paragraph, Dialog, Portal} from 'react-native-paper';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import globalStyles from '../../styles/global';  
import RNPickerSelect, {defaultStyles} from 'react-native-picker-select';




const Paso2 = ({fecha, tipoEvento, localidad, ciudad, lugar,
    guardarFecha, guardarTipoEvento, guardarLocalidad, guardarCiudad, guardarLugar}) => {
    
    // Date Picker
    // -------------------------------------------------------------------------
    
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        console.log("Se ha seleccionado una fecha: ", date);
        const opciones = {year:'numeric', month: 'numeric', day:"2-digit" };
        console.log(date.toLocaleDateString('es-ES', opciones)); 
        guardarFecha(date.toLocaleDateString('es-ES', opciones)); 
        hideDatePicker();
    };

    
    // Tipo Eventos
    // -------------------------------------------------------------------------




    return ( 
        
        <View style={globalStyles.contenedor}>

            {/* Título  */}
            <Headline 
                style={globalStyles.titulo}
            >
                Datos Evento
            </Headline> 
         

            {/* Selección de fecha */}
            
            <Text style={styles.texto}> Fecha: <Subheading>{fecha}</Subheading></Text>
            

            <Button title="Seleccionar Fecha"  onPress={showDatePicker} />
            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                locale="es_ES"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
            /> 
            <View>

                
            {/* Selección de tipo de evento */}
            
            <View style={{
                flexDirection:'row',
                marginTop:20

            }}> 
                <Text style={{ fontSize: 18, alignSelf:'center'}}> Tipo de evento:</Text>
                <View style={{flex:1}}>

                <RNPickerSelect
                    onValueChange={(value) => guardarTipoEvento(value)} 
                    
                    placeholder={{
                        label: 'Selecciona tipo de evento',
                        value: null,
                        color: 'blue',
                    }}
                    style={{ 
                        //   inputAndroid: [DefaultStyles.bodyTextBlack],
                        placeholder: {
                            // fontSize: hp("2%"),
                            textAlign: "center",
                            fontFamily: "Raleway-Semibold",
                            placeholderColor: 'red',
                            
                        },
                        inputAndroid: {
                            color: 'black', 
                        },
                    }}
                    items={[
                        { label: 'Boda', value: 'boda' },
                        { label: 'Bautizo', value: 'bautizo' },
                        { label: 'Comunión', value: 'comunion' },
                        { label: 'Cumpleaños', value: 'cumpleaños' },
                        { label: 'Evento Privado', value: 'evento_privado' },
                        { label: 'Evento Público', value: 'evento_publico' },
                        { label: 'Otro', value: 'otro'}
                    ]}
                    />
                    
                    </View>

                </View>

            </View>

            {/* Localidad */}
            
            <TextInput
                label="Localidad"
                placeholder="Introduce la localidad..."
                style={styles.input}
                onChangeText={texto => guardarLocalidad(texto)}
                value={localidad}
            />       

            {/* Ciudad */}
            
            <TextInput
                label="Ciudad"
                placeholder="Introduce la ciudad..."
                style={styles.input}
                onChangeText={texto => guardarCiudad(texto)}
                value={ciudad}
            /> 

            {/* Lugar */}
            
            <TextInput
                label="Lugar"
                placeholder="Introduce el sitio de celebración..."
                style={styles.input}
                onChangeText={texto => guardarLugar(texto)}
                value={lugar}
            /> 
            

        </View>


     );
}


const styles = StyleSheet.create ({
    texto: {
        marginBottom: 20,
        fontSize: 18
    },
    flex: {
        flex:1
    },
    boton: {
        marginTop: 100,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: '#C22222'
    },
    input:{
        marginBottom: 20,
        backgroundColor: 'transparent'
    }
})


export default Paso2;