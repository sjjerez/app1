import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { Keyboard, Text, View, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { TextInput, Headline, Button, Paragraph, Dialog, Portal} from 'react-native-paper';
import globalStyles from '../../styles/global'; 
import Button_1_person from '../../assets/images/button_1_person';
import Button_2_person from '../../assets/images/button_2_person'; 
 

const Paso1 = ({ nombre1,nombre2,telefono,correo, guardarNombre1, guardarNombre2, 
                guardarTelefono, guardarCorreo}) => { 

                    
    // Otros states necesarios en el formulario
    const [alerta, guardarAlerta] = useState(false); 



    // Boton de 1 o 2 clientes
    // -------------------------------------------------------------------------
    
    const color_activo = "#4584DA";
    const color_inactivo= "#CDCDCD";
    var color_b1 = color_activo;
    var color_b2 = color_inactivo;
    

    const cambiarColor = () => { 
        guardarBotonActivo((botonActivo + 1 ) % 2 );
    }

    const [botonActivo, guardarBotonActivo] = useState(0); 
     
 
    return (  
        <View style={globalStyles.contenedor}>
            <Headline 
                style={globalStyles.titulo}
            >
                Datos Cliente
            </Headline>
            
            <View style={{ flexDirection:'row', justifyContent:'center', height:50}}>

                <Button_1_person style={{margin:0}} width="100" height="50" 
                fill={botonActivo === 0 ? color_b1 : color_b2 } 
                fillSecondary="white" 
                 onPress= {botonActivo === 1 ? ()=>cambiarColor() : null } />  
                <Button_2_person style={{margin:0}} width="100" height="50" 
                fill={botonActivo === 0 ? color_b2 : color_b1 } 
                fillSecondary="white" 
                onPress= {botonActivo === 0 ? ()=>cambiarColor() : null } />  
                
            </View>
            <TextInput
                label={botonActivo ===0 ? "Nombre" : "Nombre Contacto" }
                placeholder="Nombre"
                style={styles.input}
                onChangeText={texto => guardarNombre1(texto)}
                value={nombre1}
            />  
            {botonActivo ===1? 
                <TextInput
                    label="Nombre Pareja"
                    placeholder="Nombre"
                    style={styles.input}
                    onChangeText={texto => guardarNombre2(texto)}
                    value={nombre2}
                />  
            :null}
            <TextInput
                label="Teléfono"
                keyboardType="numeric"
                placeholder="666666666"
                style={styles.input}
                onChangeText={texto => guardarTelefono(texto)}
                value={telefono}
            /> 
            <TextInput
                label="Correo"
                placeholder="este@ejemplo.com"
                style={styles.input}
                onChangeText={texto => guardarCorreo(texto)}
                value={correo}
            /> 
 
            <Portal>
                <Dialog
                    visible={alerta}
                    onDismiss={() => guardarAlerta(false)}
                >
                    <Dialog.Title>Error</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Todos los campos son obligatorios</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => guardarAlerta(false)}>Ok</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View> 
     );
}
 

// Estilos propios de este formulario
const styles = StyleSheet.create({
    input:{
        marginBottom: 20,
        backgroundColor: 'transparent'
    }
})

export default Paso1;