import React, {useState, useEffect} from 'react';
import {View, FlatList, Text} from 'react-native';
import TarjetaServicio from '../ui/TarjetaServicio'
import { List, Headline, Button, FAB, Searchbar, Title } from 'react-native-paper';
import globalStyles from '../../styles/global'

import Carousel from "react-native-carousel-control";


const Paso3 = ({servicios, serviciosDeseados, guardarServiciosDeseados, presupuesto, guardarPresupuesto}) => {
 
    
    
    return ( 
 
            <>
 
            <Headline style={globalStyles.titulo}>
                Selecciona servicios
            </Headline>

            <Carousel> 
                {servicios.map(servicio => <TarjetaServicio key={servicio.id} servicio={servicio}  
                    serviciosDeseados={serviciosDeseados} guardarServiciosDeseados = {guardarServiciosDeseados}
                    guardarPresupuesto={guardarPresupuesto} /> 
                    )
                }    
            </Carousel>
             
            
            <Text style={{textAlign:'center', marginTop:20}}>Presupuesto estimado:</Text>
            <Title style={{textAlign:'center'}}> {presupuesto} €</Title>
            </>


     );
}
 
export default Paso3;