import React from 'react';
import {List, Text, Title, Subheading} from 'react-native-paper';  
import {StyleSheet,View, Dimensions, TouchableOpacity} from 'react-native'; 
import Diamondring from '../../assets/images/diamondring';
import Bautizo from '../../assets/images/bautizo';
import Comunion from '../../assets/images/comunion';
import Cumpleanos from '../../assets/images/cumpleanos';
import Privado from '../../assets/images/privado';
import Publico from '../../assets/images/publico';
import Otro from '../../assets/images/otro';
import Dj_icon from '../../assets/images/dj_icon';
import Fotomaton_icon from '../../assets/images/fotomaton_icon';
import MesaDulce_icon from '../../assets/images/MesaDulce_icon';


const TarjetaCliente = ({navigation, cliente, guardarConsultarAPI, servicios}) => {
     
    const getIcon = (tE) => {
        if(tE==="boda"){
            return <Diamondring width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="bautizo"){
            return <Bautizo width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="comunion"){
            return <Comunion width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="cumpleaños"){
            return <Cumpleanos width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="evento_privado"){
            return <Privado width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="evento_publico"){
            return <Publico width="80%" height="80%" viewBox={`0 0 512 512`}/>
        } else if(tE==="otro"){
            return <Otro width="80%" height="80%" viewBox={`0 0 512 512`}/>
        }
    } 

    const calculaPrecio = () => {

    }

    return ( 
        <TouchableOpacity style={styles.cajaPrincipal} onPress={ () => navigation.navigate("DetallesCliente", {cliente:cliente, guardarConsultarAPI:guardarConsultarAPI} )}>
            
            <View style={styles.cajaHorizontal}> 
                <View style={{flex:1}}> 
                    <Title>{cliente.nombre1}</Title>
                    <Subheading>{cliente.fecha}</Subheading>
                </View>
                <View style={styles.svgBox} >
                    {getIcon(cliente.tipoEvento)}
                </View>
                <View style={{flex:1}}>
                    <Title style={{textAlign:'right'}}> {cliente.presupuesto} €</Title>
                    <Subheading style={{textAlign:'right'}}> {cliente.ciudad} </Subheading>

                </View>
            </View>

            <View            
                style={styles.separador}
            />
            <View style={{ flexDirection:'row', justifyContent:'center'}}>
                {cliente.serviciosDeseados["DJ"] ? <Dj_icon style={{margin:10}} width="20" height="20"></Dj_icon> : null }
                {cliente.serviciosDeseados["Fotomatón"] ? <Fotomaton_icon style={{margin:10}} width="20" height="20"></Fotomaton_icon> : null }
                {cliente.serviciosDeseados["Mesa Dulce"] ? <MesaDulce_icon style={{margin:10}} width="20" height="20"></MesaDulce_icon> : null }
            </View>

            {/* <List.Item
                title={cliente.nombre1}
                description = {cliente.telefono}
                
            />  */}
        </TouchableOpacity>
        // <Text>Mierda</Text>
     );
}


// ESTILOS DE COMPONENTE
// -----------------------------------------------------------------------------
const styles = StyleSheet.create({
  cajaPrincipal: {
    backgroundColor: "white",
    marginTop: 20,
    marginBottom: 10,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
      
  },
  cajaHorizontal: {
      flexDirection:'row',
      justifyContent: 'space-between',
      margin:10,
    //   backgroundColor: "blue",
  }, 
  separador:{
        borderBottomColor: 'black',
        borderBottomWidth: 0.2,
        marginLeft:40,
        marginRight:40,
    },
    svgBox: {
        aspectRatio: 1, 
        // backgroundColor: 'blue', 
        justifyContent: 'center', 
        alignItems: 'center'
    }

});

 
export default TarjetaCliente;