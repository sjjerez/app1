import React from 'react'; 
import {List, Text, Title, Subheading, Button} from 'react-native-paper';  
import {StyleSheet,View, Switch , Dimensions, Image, TouchableOpacity} from 'react-native'; 
import Diamondring from '../../assets/images/diamondring';
import Dj_icon from '../../assets/images/dj_icon';

const TarjetaServicio = ({servicio, serviciosDeseados, guardarServiciosDeseados, guardarPresupuesto, guardarActualizar}) => {
    

    const actualizar = () => {
        var resultado;
        if(serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
            resultado=950;
        }else if(serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
            resultado=800;            
        }else if(serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
            resultado=450;            
        }else if(!serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && !serviciosDeseados["Mesa Dulce"]){
            resultado=450;            
        }else if(!serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
            resultado=300;            
        }else if(serviciosDeseados["DJ"] && !serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
            resultado=700;            
        }else if(!serviciosDeseados["DJ"] && serviciosDeseados["Fotomatón"] && serviciosDeseados["Mesa Dulce"]){
            resultado=700;            
        }
        guardarPresupuesto(resultado);
    } 

    // Switch de seleccion
    const [isSwitchOn, setIsSwitchOn] = React.useState(false);
    const onToggleSwitch = () => {
        setIsSwitchOn(!isSwitchOn)
        var aux = serviciosDeseados;
        aux[servicio.nombre] = !aux[servicio.nombre];
        guardarServiciosDeseados(aux);  
        actualizar(); 
    };

    

    return (
        <View style={styles.cajaPrincipal}>
            <TouchableOpacity style={{flex:1}} onPress={ () => 
                                navigation.navigate("DetallesServicio", {servicio:servicio, 
                                guardarConsultarAPI:guardarConsultarAPI} )}>
                <Image resizeMode="cover" style={{flex:1, width:'auto', height:200}} 
                        source={servicio.nombre==="DJ" 
                            ?  require('../../assets/images/wallpaper_dj.jpg') 
                            :   (servicio.nombre==="Mesa Dulce" 
                                ? require('../../assets/images/wallpaper_mesa_dulce.jpg') 
                                : require('../../assets/images/wallpaper_fotomaton.jpg') 
                                )
                            } />                
            </TouchableOpacity>
            <Title style={{ marginTop:15, marginLeft:20}}>{servicio.nombre}</Title>
            <Subheading style={{textAlign:'left', marginLeft:20}}>Precio desde <Subheading>150€</Subheading></Subheading>
            <View style={{ marginLeft:20, marginRight:20, marginBottom:20, flexDirection:'row', justifyContent:'space-between'}}> 
                <View style={{ backgroundColor:"white"}}>
                    {/* <Text>Contratar</Text> */}
                    <Switch style={{marginTop:8, transform: [{ scaleX: 1.3 }, { scaleY: 1.3}] }}
                        value={isSwitchOn} onValueChange={onToggleSwitch} />
                </View>

                <View style={{ backgroundColor:"white" , alignSelf:'flex-end'}}>
                    <Button 
                        style={styles.boton} 
                        mode="contained" 
                        
                        onPress={() => mostrarConfirmacion() }
                        >
                        Detalles
                    </Button> 
                </View>
                
            </View>

        </View>
      );
}
 

// ESTILOS DE COMPONENTE
// -----------------------------------------------------------------------------
const styles = StyleSheet.create({
    cajaPrincipal: {
      backgroundColor: "white",
      borderRadius:5,
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,  
      elevation: 5,        
    },
    cajaHorizontal: {
        flexDirection:'row',
        justifyContent: 'space-between',
        margin:10,
      //   backgroundColor: "blue",
    }, 
    separador:{
          borderBottomColor: 'black',
          borderBottomWidth: 0.2,
          marginLeft:40,
          marginRight:40,
      },
      svgBox: {
          aspectRatio: 1, 
          // backgroundColor: 'blue', 
          justifyContent: 'center', 
          alignItems: 'center'
      },
      boton: {  
          backgroundColor: '#4584DA'
      }
  
  });
  
    
export default TarjetaServicio;