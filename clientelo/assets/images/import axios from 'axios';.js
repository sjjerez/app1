import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { View, StyleSheet } from 'react-native';
import { TextInput, Headline, Button, Paragraph, Dialog, Portal} from 'react-native-paper';
import globalStyles from '../styles/global'; 
import Button_1_person from '../assets/images/button_1_person';
import Button_2_person from '../assets/images/button_2_person';


const NuevoCliente = ({navigation, route}) => {
    
    




    const {guardarConsultarAPI} = route.params;

    // Campos del formulario
    const [nombre, guardarNombre] = useState('');
    const [telefono, guardarTelefono] = useState('');
    const [correo, guardarCorreo] = useState(''); 

    // Detectamos si estamos editando un cliente o creando uno nuevo
    useEffect( () =>  {
        // Si lo estamos editando, capturamos primero los valores existentes:
        if(route.params.cliente){
            const {nombre, telefono, correo, empresa} = route.params.cliente;
            guardarNombre(nombre);
            guardarTelefono(telefono);
            guardarCorreo(correo);
        }else {
            // Nuevo cliente
            
        }
    },[]);

    // Otros states necesarios en el formulario
    const [alerta, guardarAlerta] = useState(false); 

    // Almacenar cliente en la BD
    const guardarCliente = async () => {
        
        // Validación
        if(nombre === '' || telefono === '' || correo === ''){
            // console.log('Hay campos vacíos');
            guardarAlerta(true);
            return
        }
    
    
        // Generación de cliente
        const cliente = { nombre, telefono, correo};
        console.log(cliente);

        // Si estamos editando o creando un nuevo cliente
        if(route.params.cliente){
            // Estamos editando
            const {id} = route.params.cliente;
            cliente.id = id;
            const url = `http://10.0.2.2:3000/clientes/${id}`; 

            try {
                await axios.put(url, cliente);
            } catch (error) {
                console.log(error);
            }
        }else{
            // Estamos creando uno nuevo
         // Guardado del cliente en la API
            try{
                // URL específica de android
                await axios.post('http://10.0.2.2:3000/clientes', cliente);
            } catch (error){
                console.log(error);
            }
        }

        // Redirección
        navigation.navigate('Inicio');

        // Limpiar el formulario (opcional)
        guardarNombre('');
        guardarTelefono('');
        guardarCorreo('');

        // console.log('Guardando cliente');

        // Cambiamos el estate para actualizar la vista de clientes
        guardarConsultarAPI(true);
    }




    return ( 
        <View style={globalStyles.contenedor}>
            <Headline 
                style={globalStyles.titulo}
            >
                Datos Cliente
            </Headline>
            
            <View style={{ flexDirection:'row', justifyContent:'center', height:50}}>

                <Button_1_person style={{margin:0}} width="100" height="50" fill="#000" fillSecondary="#fff"  onPress={()=>console.log("holaaaa")}></Button_1_person>  
                <Button_2_person style={{margin:0}} width="100" height="50"></Button_2_person>  
            </View>
            <TextInput
                label="Nombre"
                placeholder="Sergio"
                style={styles.input}
                onChangeText={texto => guardarNombre(texto)}
                value={nombre}
            />  
            <TextInput
                label="Teléfono"
                placeholder="666666666"
                style={styles.input}
                onChangeText={texto => guardarTelefono(texto)}
                value={telefono}
            /> 
            <TextInput
                label="Correo"
                placeholder="este@ejemplo.com"
                style={styles.input}
                onChangeText={texto => guardarCorreo(texto)}
                value={correo}
            /> 

            <Button color="#447AC3" icon="pencil-circle" mode="contained" onPress={() => guardarCliente()}>
                Añadir
            </Button>

            <Portal>
                <Dialog
                    visible={alerta}
                    onDismiss={() => guardarAlerta(false)}
                >
                    <Dialog.Title>Error</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Todos los campos son obligatorios</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => guardarAlerta(false)}>Ok</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
     );
}
 

// Estilos propios de este formulario
const styles = StyleSheet.create({
    input:{
        marginBottom: 20,
        backgroundColor: 'transparent'
    }
})

export default NuevoCliente;